const express = require('express');
const passport = require('passport');
const { encodeToken } = require('../../modules/tokenize');

const { client_id, secret_key, domain, callback_url } = require('../../secret.json').auth0;
const router = express.Router();

// Perform the login
router.get(
  '/login',
  passport.authenticate('auth0', {
    scope: 'openid email profile'
  }),
  function (req, res) {
    res.redirect('/');
  }
);

// Perform session logout and redirect to homepage
router.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/');
});


// Perform the final stage of authentication and redirect to '/user'
router.get(
  '/callback',
  passport.authenticate('auth0', {
    failureRedirect: '/'
  }),
  function (req, res) {
    const token = encodeToken({ user_id: req.user.user_id });
    res.cookie('_tokenize', token);
    res.redirect('/');
  }
);

module.exports.router = router;
const jwt = require('jwt-simple');
const SECRET = 'dunice123';

function encodeToken(payload) {
  const token = jwt.encode(payload, SECRET);
  return token;
}

function decodeToken(token) {
  const decoded = jwt.decode(token, SECRET);
  return decoded;
}

module.exports = { encodeToken, decodeToken }

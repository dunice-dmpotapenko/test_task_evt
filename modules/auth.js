
const passport = require('passport');
const Auth0Strategy = require('passport-auth0');
const { client_id, secret_key, domain, callback_url } = require('../secret.json').auth0;

// Configure Passport to use Auth0
const strategy = new Auth0Strategy(
  {
    domain: domain,
    clientID: client_id,
    clientSecret: secret_key,
    callbackURL: callback_url
  },
  (accessToken, refreshToken, extraParams, profile, done) => {
    return done(null, profile);
  }
);

passport.use(strategy);

// This can be used to keep a smaller payload
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});


module.exports.passport = passport;

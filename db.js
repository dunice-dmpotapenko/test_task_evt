const Sequelize = require('sequelize');
const { host, port, user, password, name } = require('./secret.json').db;

const sequelize = new Sequelize(name, user, password, {
  host,
  dialect: 'postgres',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },

  // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
  operatorsAliases: false
});

const User = sequelize.define('user', {
  Id: {
    type: Sequelize.INTEGER(10),
    primaryKey: true,
    unique: true,
    autoIncrement: true
  },
  authid: {
    type: Sequelize.STRING(32),
    allowNull: false,
    unique: true,
  },
  paymentexternid: {
    type: Sequelize.INTEGER(50),
    defaultValue: null
  },
  email: {
    type: Sequelize.INTEGER(100),
    allowNull: false,
  },
  isadvisor: {
    type: Sequelize.STRING(1),
    defaultValue: ''
  },
  verifiedstatus: {
    type: Sequelize.STRING(1)
  },
  accountstatus: {
    type: Sequelize.STRING(1)
  },
  advisorsid: {
    type: Sequelize.INTEGER(10)
  },
  countryid: {
    type: Sequelize.INTEGER(3)
  },
  langid: {
    type: Sequelize.INTEGER(3)
  },
  domainid: {
    type: Sequelize.INTEGER(3)
  },
});

function initDb() {
  return sequelize.sync().then(_ => {
    console.log('DB is synced');
    return true;
  });
}

module.exports = {
  initDb
}
